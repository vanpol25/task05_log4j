package com.polahniuk.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * Class which implements Twilio SMS sender.
 * Has all the information you need to send SMS.
 * SMS will sends by method {@link SMSSender#send(String)}.
 *
 * @version beta 1
 * @author Ivan Polahniuk
 */
public class SMSSender {
    public static final String ACCOUNT_SID = "AC7a897eb9fb3d40fab7bcd8aec4405df4";
    public static final String AUTH_TOKEN = "481203356b8d46de8363de30e2e8237e";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380666503909"),
                        new PhoneNumber("+16464901792"), str) .create();
    }
}
