package com.polahniuk;

import com.polahniuk.animal.Cat;
import com.polahniuk.animal.Dog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class Application with entry point main().
 * Initializes of objects that include all levels of logging of log4j2.
 *
 * @version beta 1
 * @author Ivan Polahniuk
 */
public class Application {

    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        log.trace("trace");
        log.debug("debug");
        getSum(1, 7);
        getString("food");
        resSum(10);

        Cat cat = new Cat("cat");
        cat.log();
        Dog dog = new Dog("dog");
        dog.log();
    }

    public static int getSum(int a, int b) {
        log.info("info");
        return a + b;
    }

    public static String getString(String string) {
        log.warn("warn");
        if (string.length() < 10) {
            log.error("error");
        }
        return string;
    }

    public static int resSum(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        if (sum > 10) {
            log.fatal("fatal");
        }
        return sum;
    }

}
