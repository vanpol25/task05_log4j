package com.polahniuk.animal;

import org.apache.logging.log4j.*;

/**
 * Class that extends abstract class {@link Animal}.
 * Has field of logger.
 * Has overridden method {@link Dog#log}.
 * This method has all levels info of log4j2.
 *
 * @version beta 1
 * @author Ivan Polahniuk
 */
public class Dog extends Animal {

    private static final Logger log = LogManager.getLogger(Dog.class);

    public Dog(String name) {
        super(name);
        log.info("Created object Dog");
    }

    @Override
    public void log() {
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.error("error");
        log.fatal("fatal");
    }
}
