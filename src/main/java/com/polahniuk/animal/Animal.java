package com.polahniuk.animal;

/**
 * Is an abstract class witch describes animals.
 * Has an abstract method {@link Animal#log} to test log4j2.
 *
 * @version beta 1
 * @author Ivan Polahniuk
 */
public abstract class Animal {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void log();

    public Animal(String name) {
        this.name = name;
    }
}
